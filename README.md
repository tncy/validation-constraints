# Validation Constraints

*Version: 1.0.18 - 2023-10-07*


## What is it?

This is a library of constraints compatible with the [Jakarta Bean Validation](http://beanvalidation.org/) specification.
Jakarta Bean Validation defines a metadata model and API for JavaBean as well as method validation.
The default metadata source are annotations, with the ability to override and extend
the metadata through the use of XML validation descriptors.

## Documentation

The documentation for this release is included in the _docs_ directory of the distribution package or can be accessed [online](https://mvn.tncy.eu/sites/net.tncy.validator/validation-constraints/).

## Release Notes

The full list of changes for this release can be found in changelog.txt.

## System Requirements

JDK 8 or above.
Jakarta Bean Validation implementation such as [Hibernate Validator 6.2] (https://hibernate.org/validator/releases/6.2/) or above.

## Using Validation Constraints

* In case you use the distribution archive from the download site, copy _dist/validation-constraints-&lt;version&gt;.jar_ together with all
jar files from _dist/lib/required_ into the classpath of your application. 

* Add the following artifact to your Maven/Ivy/Gradle dependency list:

        <dependency>
            <groupId>net.tncy.validator</groupId>
            <artifactId>validation-constraints</artifactId>
            <version>1.0.18</version>
        </dependency>

## Licensing

Validation Constraints itself as well as the Jakarta Bean Validation API and TCK are all provided and distributed under
the Apache Software License 2.0. Refer to license.txt for more information.

## Build from Source

You can build Validation Constraints from source by cloning the git repository `https://bitbucket.org/tncy/validation-constraints`.
You will also need a JDK 17+ and Maven 3 (>= 3.3.1). With these prerequisites in place you can compile the source via:

    mvn clean install

There are more build options available as well. For more information refer to Contributing guidelines.

## Continuous Integration

The official Continuous Integration service for the project is hosted on [TNCY's CI platform](https://jenkins.tncy.eu/job/validation-constraints).


## Validation Constraints URLs

* [Home Page](https://mvn.tncy.eu/sites/net.tncy.validator/validation-constraints/)
* [Sources] (https://bitbucket.org/tncy/validation-constraints)
* [Downloads](https://nexus.tncy.eu/service/rest/repository/browse/maven-releases/net/tncy/validator/validation-constraints)
* [Continuous Integration](https://jenkins.tncy.eu/job/validation-constraints) [![Build Status](https://jenkins.tncy.eu/buildStatus/icon?job=validation-constraints)](https://jenkins.tncy.eu/job/validation-constraints/)