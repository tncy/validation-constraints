package net.tncy.validator.util;

/**
 * Helper class for digit array handling.
 * 
 * @author Xavier Roy
 */
public class ValidatorUtil {

    /** 
     * Converts a string reprensentation of a natural number (positive integer) 
     * into an array of numreic digit.
     * 
     * @param s string reprensentation of a natural number
     * @return aa array of digits (integer)
     */
    public static int[] toDigitArray(String s) {
        int[] digits = new int[s.length()];
        char[] characters = s.toCharArray();
        for (int i = 0; i < characters.length; i++) {
            char c = characters[i];
            if (Character.isDigit(c)) {
                digits[i] = Character.digit(c, 10);
            } else {
                throw new NumberFormatException("'" + c + "' does not represent a digit.");
            }
        }
        return digits;
    }

    /** 
     * Converts a natural number (positive integer) into an array of numreic 
     * digits. If provided number is a negative integer, the method will 
     * consider its absolute value.
     * 
     * @param n natural number
     * @return an array of digits (integer)
     */
    public static int[] toDigitArray(int n) {
        return toDigitArray(String.valueOf(Math.abs(n)));
    }

    /**
     * Creates a sequence of integers between two values. The bounds are 
     * included in the sequence.
     * 
     * @param from the first value of the sequence
     * @param to the last value of the sequence
     * @return the created sequence
     */
    public static int[] createSequence(int from, int to) {
        int len = Math.abs(from - to);
        int step = (to - from) / len;
        int[] seq = new int[len + 1];
        int val = from;
        for (int i = 0; i < seq.length; i++) {
            seq[i] = val;
            val += step;
        }
        assert seq[len] == to;
        return seq;
    }

    /**
     * Multiplies every number from the source array by the factor at the same
     * index within the weight array
     *
     * @param source original digit array
     * @param weights factor used to multply otiginal digit
     * @return the weighted number array
     */
    public static int[] multiply(int[] source, int[] weights) {
        if (source.length != weights.length) {
            throw new IllegalArgumentException("Length of source and weights parameters doesn't match.");
        }
        int[] products = new int[source.length];
        for (int i = 0; i < products.length; i++) {
            products[i] = source[i] * weights[i];
        }
        return products;
    }

    /**
     * Sums values of an array of integers.
     * 
     * @param values the values to sum
     * @return the sum of the provided values
     */
    public static int sum(int[] values) {
        int sum = 0;
        for (int value : values) {
            sum += value;
        }
        return sum;
    }

    /**
     * Adds the values of an integer table with poderation according to whether they occupy an odd or even position.
     * 
     * @param values the values to sum
     * @param oddWeight the weight of numbers at odd positions
     * @param evenWeight the weight of numbers at even positions
     * @return the weighted sum
     */
    public static int sumWithWeight(int[] values, int oddWeight, int evenWeight) {
        int sum = 0;
        int oddSum = 0;
        int evenSum = 0;
               
        for (int i = 0; i < values.length; i++) {
            // As we start at index 0, even and odd positions are inverted
            if (i % 2 == 0) {
                oddSum += values[i];
            } else {
                evenSum += values[i];
            }
        }
        sum = oddSum*oddWeight + evenSum*evenWeight;
        return sum;
    }

    /**
     * Sums the digits of an interger number.
     * 
     * @param value an integer number
     * @return the sum of the digits
     */
    public static int sumDigits(int value) {
        int[] digits = toDigitArray(value);
        return sum(digits);
    }

    /**
     * Sums the digits of an interger number.
     * 
     * @param value an integer number
     * @return the sum of the digits
     */
    public static int digitalize(int value) {
        int digit = 0;
        if (value < 10) {
            digit = value;
        } else {
            digitalize(sumDigits(value));
        }
        return digit;
    }
}
