package net.tncy.validator.constraints.books;

public enum ISBNFormat {
    
    ISBN_10(10), ISBN_13(13);
    
    private final int length;
    
    private ISBNFormat(int length) {
        this.length = length;
    }
    
    public int getLength() {
        return this.length;
    }
    
}
