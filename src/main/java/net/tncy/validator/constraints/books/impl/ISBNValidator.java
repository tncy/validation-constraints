package net.tncy.validator.constraints.books.impl;

import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import net.tncy.validator.constraints.books.ISBN;
import net.tncy.validator.constraints.books.ISBNFormat;
import net.tncy.validator.util.ValidatorUtil;

/**
 *
 * @author Xavier ROY
 */
public class ISBNValidator implements ConstraintValidator<ISBN, String> {

    private static final int ZERO = 0;
    private static final int ONE = 1;
    private static final int TWO = 2;
    private static final int THREE = 3;
    private static final int TEN = 10;
    private static final int ELEVEN = 11;
    private static final int THIRTEEN = 13;
    private static final char X_CHAR = 'X';
    private static final String SEG_SEPARATOR = "-";
    private static final String EMPTY_STRING = "";
    private static final int ISBN10_LEN = TEN;
    private static final int ISBN13_LEN = THIRTEEN;
    private static final int ISBN10_CHECK_DIGIT_POS = ISBN10_LEN - 1;
    private static final int ISBN13_CHECK_DIGIT_POS = ISBN13_LEN - 1;
    private static final Pattern ISBN10_PATTERN = Pattern.compile("\\d{9}(\\d|X)");
    private static final Pattern ISBN13_PATTERN = Pattern.compile("97(8|9)\\d{9}(\\d|X)");

    private ISBNFormat format = ISBNFormat.ISBN_13;
    private boolean explain = false;

    @Override
    public void initialize(ISBN constraintAnnotation) {
        this.initialize(constraintAnnotation.format(), constraintAnnotation.explain());
    }

    final void initialize(ISBNFormat format, boolean explain) {
        this.explain = explain;
        this.format = format;
    }

    @Override
    public boolean isValid(String bookNumber, ConstraintValidatorContext constraintContext) {
        if (explain) {
            constraintContext.disableDefaultConstraintViolation();
        }
        boolean valid = false;
        if (bookNumber == null || bookNumber.trim().isEmpty()) {
            valid = true;
        } else {
            String bn = bookNumber.replace(SEG_SEPARATOR, EMPTY_STRING).trim();
            switch (this.format) {
                case ISBN_10:
                    valid = isValidISBN10(bn, constraintContext);
                    break;
                case ISBN_13:
                default:
                    // Since 2007, ISBN-13 is the standard format (used by default).
                    valid = isValidISBN13(bn, constraintContext);
            }
        }
        return valid;
    }

    private boolean isValidISBN10(String bookNumber, ConstraintValidatorContext constraintContext) {
        boolean valid = false;
        int len = bookNumber.length();
        if (len == ISBN10_LEN) {
            Matcher matcher = ISBN10_PATTERN.matcher(bookNumber);
            if (!matcher.matches()) {
                explain(constraintContext, "net.tncy.validator.constraints.book.ISBN.patternViolation");
            } else {
                char checkDigit = bookNumber.charAt(ISBN10_CHECK_DIGIT_POS);
                try {
                    int checkDigitValue = computeCheckDigitValue(checkDigit);
                    int[] digits = ValidatorUtil.toDigitArray(bookNumber.substring(ZERO, ISBN10_CHECK_DIGIT_POS));
                    int[] weights = ValidatorUtil.createSequence(TEN, TWO);
                    int[] products = ValidatorUtil.multiply(digits, weights);
                    int sum = ValidatorUtil.sum(products);
                    valid = checkDigitValue == (ELEVEN - (sum % ELEVEN)) % ELEVEN;
                } catch (NumberFormatException e) {
                    explain(constraintContext, "net.tncy.validator.constraints.book.ISBN.invalidCharacter");
                }
            }
        } else {
            explain(constraintContext, "net.tncy.validator.constraints.book.ISBN.invalidLength");
        }
        return valid;
    }

    private boolean isValidISBN13(String bookNumber, ConstraintValidatorContext constraintContext) {
        boolean valid = false;
        int len = bookNumber.length();
        if (len == ISBN13_LEN) {
            Matcher matcher = ISBN13_PATTERN.matcher(bookNumber);
            if (!matcher.matches()) {
                explain(constraintContext, "net.tncy.validator.constraints.book.ISBN.patternViolation");
            } else {
                char checkDigit = bookNumber.charAt(ISBN13_CHECK_DIGIT_POS);
                try {
                    int checkDigitValue = computeCheckDigitValue(checkDigit);
                    int[] digits = ValidatorUtil.toDigitArray(bookNumber.substring(ZERO, ISBN13_CHECK_DIGIT_POS));
                    int sum = ValidatorUtil.sumWithWeight(digits, ONE, THREE);
                    valid = checkDigitValue == (TEN - (sum % TEN)) % TEN;
                } catch (NumberFormatException e) {
                    explain(constraintContext, "net.tncy.validator.constraints.book.ISBN.invalidCharacter");
                }
            }
        } else {
            explain(constraintContext, "net.tncy.validator.constraints.book.ISBN.invalidLength");
        }
        return valid;
    }

    private int computeCheckDigitValue(char c) {
        int value;
        if (Character.isDigit(c)) {
            value = Character.digit(c, TEN);
        } else if (c == X_CHAR) {
            value = TEN;
        } else {
            throw new NumberFormatException("'" + c + "' does not represent a digit.");
        }
        return value;
    }

    private void explain(ConstraintValidatorContext constraintContext, String messageKey) {
        if (explain) {
            constraintContext.buildConstraintViolationWithTemplate("{" + messageKey + "}").addConstraintViolation();
        }
    }
}
