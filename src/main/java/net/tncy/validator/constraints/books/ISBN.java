package net.tncy.validator.constraints.books;

import static java.lang.annotation.ElementType.ANNOTATION_TYPE;
import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;
import net.tncy.validator.constraints.books.impl.ISBNValidator;

/**
 * ISO 2108
 *
 * @see <a href="http://en.wikipedia.org/wiki/ISBN">ISBN sur Wikipedia</a>
 * @author xavier
 */
@Target({METHOD, FIELD, ANNOTATION_TYPE})
@Retention(RUNTIME)
@Constraint(validatedBy = ISBNValidator.class)
@Documented
public @interface ISBN {

    String message() default "{net.tncy.validator.constraints.books.ISBN}";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
    
    ISBNFormat format() default ISBNFormat.ISBN_13;

    boolean explain() default false;
}
