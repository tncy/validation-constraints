package net.tncy.validator.constraints.bank.impl;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import net.tncy.validator.constraints.bank.BIC;

/**
 * Regex : ([a-zA-Z]{4}[a-zA-Z]{2}[a-zA-Z0-9]{2}([a-zA-Z0-9]{3})?)
 * @author student
 */
public class BICValidator implements ConstraintValidator<BIC, String> {

    private static final int BIC_MIN_LEN = 8;
    private static final int BIC_MAX_LEN = 11;
    private boolean explain = false;

    @Override
    public void initialize(BIC constraintAnnotation) {
        explain = constraintAnnotation.explain();
    }

    @Override
    public boolean isValid(String bankIdentifier, ConstraintValidatorContext constraintContext) {
        if (explain) {
            constraintContext.disableDefaultConstraintViolation();
        }
        boolean isValid = false;
        if (bankIdentifier == null || bankIdentifier.trim().isEmpty()) {
            isValid = true;
        } else {
            int len = bankIdentifier.length();
            if (len == BIC_MIN_LEN || len == BIC_MAX_LEN) {
                String bankCode = bankIdentifier.substring(0, 4); // 4 letters
                String countryCode = bankIdentifier.substring(4, 6); // 2 letters - ISO 3166-1 A2
                String locationCode = bankIdentifier.substring(6, 8); // 2 letters or digits
                String branchCode = (len > 8 ? bankIdentifier.substring(8, len - 1) : null); // 3 letters or digits
                if (bankCode.matches("[a-zA-Z]{4}")) {
                    if (countryCode.matches("[a-zA-Z]{2}")) { // TODO : Check ISO 3166-1 A2 compliance
                        if (locationCode.matches("[a-zA-Z0-9]{2}")) {
                            if (branchCode != null) {
                                if (branchCode.matches("[a-zA-Z0-9]{3}")) {
                                    if (explain) {
                                        constraintContext.buildConstraintViolationWithTemplate("{net.tncy.validator.constraints.bank.BIC.malformedBranchCode}").addConstraintViolation();
                                    }
                                } else {
                                    isValid = true;
                                }
                            } else {
                                isValid = true;
                            }
                        } else {
                            if (explain) {
                                constraintContext.buildConstraintViolationWithTemplate("{net.tncy.validator.constraints.bank.BIC.malformedLocationCode}").addConstraintViolation();
                            }
                        }
                    } else {
                        if (explain) {
                            constraintContext.buildConstraintViolationWithTemplate("{net.tncy.validator.constraints.bank.BIC.invalidCountryCode}").addConstraintViolation();
                        }
                    }
                } else {
                    if (explain) {
                        constraintContext.buildConstraintViolationWithTemplate("{net.tncy.validator.constraints.bank.BIC.malformedBankCode}").addConstraintViolation();
                    }
                }
            } else {
                if (explain) {
                    constraintContext.buildConstraintViolationWithTemplate("{net.tncy.validator.constraints.bank.BIC.invalidBICLength}").addConstraintViolation();
                }
            }
        }
        return isValid;
    }
}
