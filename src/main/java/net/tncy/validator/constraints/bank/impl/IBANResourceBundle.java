package net.tncy.validator.constraints.bank.impl;

import java.util.ListResourceBundle;

public class IBANResourceBundle extends ListResourceBundle {

    @Override
    protected Object[][] getContents() {
        Object[][] content = {
            {"DE", 22},
            {"AD", 24},
            {"AT", 20},
            {"BE", 16},
            {"BA", 20},
            {"BG", 22},
            {"HR", 21},
            {"CY", 28},
            {"CZ", 24},
            {"DK", 18},
            {"ES", 24},
            {"EE", 20},
            {"FO", 18},
            {"FI", 18},
            {"FR", 27},
            {"GI", 23},
            {"GB", 22},
            {"GR", 27},
            {"GL", 18},
            {"HU", 28},
            {"IS", 26},
            {"IE", 22},
            {"IT", 27},
            {"LV", 21},
            {"LI", 21},
            {"LT", 20},
            {"LU", 20},
            {"MK", 19},
            {"MT", 31},
            {"MC", 27},
            {"NL", 18},
            {"NO", 15},
            {"PL", 28},
            {"PT", 25},
            {"RO", 24},
            {"SM", 27},
            {"RS", 22},
            {"CS", 22},
            {"SK", 24},
            {"SI", 19},
            {"SE", 24},
            {"CH", 21},
            {"TR", 26},
            {"TN", 24}
        };
        return content;
    }
}
