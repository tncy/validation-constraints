package net.tncy.validator.constraints.bank.impl;

import java.lang.Integer;
import java.util.ArrayList;
import java.util.List;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import net.tncy.validator.constraints.bank.ISIN;

/**
 * @author xavier
 * @see <a href="http://fr.wikipedia.org/wiki/ISO_6166">ISO 6166</a>
 */
public class ISINValidator implements ConstraintValidator<ISIN, String> {

    private static final int ISIN_LEN = 12;
    private static final int CTRL_KEY_POS = ISIN_LEN - 1;
    private static final int NSIN_START_POS = 2;
    private static final int ASCII_OFFSET = 55;
    private boolean explain = false;

	 @Override
    public void initialize(ISIN constraintAnnotation) {
        explain = constraintAnnotation.explain();
    }

	 @Override
    public boolean isValid(String isin, ConstraintValidatorContext constraintContext) {
        if (explain) {
            constraintContext.disableDefaultConstraintViolation();
        }
        boolean valid = false;
        if (isin == null || isin.trim().isEmpty()) {
            valid = true;
        } else {
            int len = isin.length();
            if (len == ISIN_LEN) {
                String countryCode = isin.substring(0, NSIN_START_POS); // 2 letters - ISO 3166-1 A2 + XS
                // TODO: Validate countryCode
                String nsin = isin.substring(NSIN_START_POS, CTRL_KEY_POS); // 9 characters
                // TODO: Validate NSIN (depends on contryCode)
                Integer controlKey = new Integer(isin.substring(CTRL_KEY_POS));
                List<Integer> digits = toDigitArray(isin.substring(0, CTRL_KEY_POS));
                List<Integer> results = new ArrayList<Integer>();
                boolean odd = digits.size() % 2 == 1;
                for (Integer digit : digits) {
                    if (odd) {
                        results.add(digit * 2);
                    } else {
                        results.add(digit);
                    }
                    odd = !odd;
                }
                results = toDigitArray(results);
                int sum = sum(results);
                int ciel = ciel(sum);
                if (ciel - sum == controlKey) {
                    valid = true;
                } else {
                    if (explain) {
                        constraintContext.buildConstraintViolationWithTemplate("{net.esial.validator.constraints.bank.ISIN.controlDigitFailed}").addConstraintViolation();
                    }
                }
            } else {
                if (explain) {
                    constraintContext.buildConstraintViolationWithTemplate("{net.esial.validator.constraints.bank.ISIN.invalidISINLength}").addConstraintViolation();
                }
            }
        }
        return valid;
    }

    private List<Integer> toDigitArray(String s) {
        List<Integer> digits = new ArrayList<Integer>();
        for (char c : s.toCharArray()) {
            digits.addAll(toDigitArray(c));
        }
        return digits;
    }

    private List<Integer> toDigitArray(Integer i) {
        List<Integer> digits = new ArrayList<Integer>();
        for (char c : i.toString().toCharArray()) {
            assert Character.isDigit(c);
            digits.add(Character.digit(c, 10));
        }
        return digits;
    }

    private List<Integer> toDigitArray(Character c) {
        List<Integer> digits = new ArrayList<Integer>();
        if (Character.isDigit(c)) {
            digits.add(Character.digit(c, 10));
        } else if (Character.isLetter(c)) {
            int i = Character.digit(c, Character.MAX_RADIX);
            digits.addAll(toDigitArray(i));
        }
        return digits;
    }

    private List<Integer> toDigitArray(List<Integer> list) {
        List<Integer> digits = new ArrayList<Integer>();
        for (Integer i : list) {
            digits.addAll(toDigitArray(i));
        }
        return digits;
    }

    private int sum(List<Integer> results) {
        int s = 0;
        for (Integer i : results) {
            s += i;
        }
        return s;
    }

    private int ciel(int n) {

        int r = n % 10;
        if (r == 0) {
            return n;
        } else {
            return n - r + 10;
        }
    }
}
