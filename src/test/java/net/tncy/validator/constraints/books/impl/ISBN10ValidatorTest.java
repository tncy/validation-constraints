package net.tncy.validator.constraints.books.impl;

import net.tncy.validator.constraints.books.ISBNFormat;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author xavier
 */
    public class ISBN10ValidatorTest {

    private static ISBNValidator validator;

    @BeforeClass
    public static void setUpClass() throws Exception {
        validator = new ISBNValidator();
        validator.initialize(ISBNFormat.ISBN_10, false);
    }

    @Test
    public void testNullAndEmptyString() throws Exception {
        isValid("");
        isValid(null);
    }

    @Test
    public void testValid() throws Exception {
        isValid("2266111566");
        isValid("99921-58-10-7");
        isValid("9971-5-0210-0");
        isValid("960-425-059-0");
        isValid("80-902734-1-6");
        isValid("85-359-0277-5");
        isValid("1-84356-028-3");
        isValid("0-684-84328-5");
        isValid("0-8044-2957-X");
        isValid("0-85131-041-9");
        isValid("0-943396-04-2");
        isValid("0-9752298-0-X");
    }

    @Test
    public void testInvalidLength() throws Exception {
        isNotValid("85-359-027-5"); // Fails on length verification (too short)
        isNotValid("85-359-02477-5"); // Fails on length verification (too long)
    }

    @Test
    public void testInvalidCrtlKey() throws Exception {
        isNotValid("85-359-0277-8"); // Fails on control key verification
    }

    @Test
    public void testForbiddenCharacters() throws Exception {
        isNotValid("85-359-0277-*");
        isNotValid("85-3A9-0277-5");

    }

    private void isValid(String s) {
        assertTrue("Expected a valid ISBN.", validator.isValid(s, null));
    }

    private void isNotValid(String s) {
        assertFalse("Expected a invalid ISBN.", validator.isValid(s, null));
    }
}