package net.tncy.validator.constraints.books.impl;

import net.tncy.validator.constraints.books.ISBNFormat;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author xavier
 */
    public class ISBN13ValidatorTest {

    private static ISBNValidator validator;

    @BeforeClass
    public static void setUpClass() throws Exception {
        validator = new ISBNValidator();
        validator.initialize(ISBNFormat.ISBN_13, false);
    }

    @Test
    public void testNullAndEmptyString() throws Exception {
        isValid("");
        isValid(null);
    }

    @Test
    public void testValid() throws Exception {
        isValid("9-782940-199617");
        isValid("9780132350884");
        isValid("978-0201633610");
        isValid("978-27440-24948");
    }

    @Test
    public void testInvalidLength() throws Exception {
        isNotValid("978-27440-2494"); // Fails on length verification (too short)
        isNotValid("978-274640-24948"); // Fails on length verification (too long)
    }

    @Test
    public void testInvalidCrtlKey() throws Exception {
        isNotValid("978-27440-24946"); // Fails on control key verification
    }

    @Test
    public void testForbiddenCharacters() throws Exception {
        isNotValid("978-27440-2494*");
        isNotValid("978-274A0-24948");

    }

    private void isValid(String s) {
        assertTrue("Expected a valid ISBN.", validator.isValid(s, null));
    }

    private void isNotValid(String s) {
        assertFalse("Expected a invalid ISBN.", validator.isValid(s, null));
    }
}