package net.tncy.validator.constraints.devices.impl;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import org.junit.BeforeClass;
import org.junit.Test;

/**	
 * Test case for IMEI Validator
 *
 * @author x-roy
 */
public class IMEIValidatorTest {

	private static IMEIValidator validator;

	@BeforeClass
	public static void setUpClass() {
		validator = new IMEIValidator();
	}

	@Test
	public void testNullAndEmptyString() throws Exception {
		isValid("");
		isValid(null);
	}

	@Test
	public void testValid() throws Exception {
		isValid("357109070090210"); // Samsung Galaxy Tab S2
		isValid("357559018415996"); // HTC Diamond Touch
		isValid("013412001320144"); // Apple iPhone 5 16G
		isValid("013938000417135"); // Lenovo ThinkPad 10
	}

	@Test
	public void testInvalidLength() throws Exception {
		isNotValid("13412001320144"); // Fails on length verification (too short)
		isNotValid("0134120013201446598"); // Fails on length verification (too long)
	}
	
	@Test
	public void testInvalidCheckDigit() throws Exception {
		isNotValid("254872054800044");
	}

	private void isValid(String value) {
		assertTrue("Expected a valid IMEI.", validator.isValid(value, null));
	}

	private void isNotValid(String value) {
		assertFalse("Expected an invalid IMEI.", validator.isValid(value, null));
	}
}
