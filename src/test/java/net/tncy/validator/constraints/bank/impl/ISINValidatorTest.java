package net.tncy.validator.constraints.bank.impl;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author xavier
 */
public class ISINValidatorTest {

    private static ISINValidator validator;

    @BeforeClass
    public static void setUpClass() throws Exception {
        validator = new ISINValidator();
    }

    @Test
    public void testNullAndEmptyString() throws Exception {
        isValid("");
        isValid(null);
    }

    @Test
    public void testValidISIN() throws Exception {
        isValid("FR0000133308"); // France Telecom
        isValid("FR0003500008"); // Indice CAC 40
        isValid("US5949181045"); // Microsoft Corp.
        isValid("DE000BAY0017"); // Bayer AG
        isValid("US4592001014"); // IBM
    }

    @Test
    public void testInvalidISINLength() throws Exception {
        isNotValid("FR0000"); // Fails on length verification (too short)
        isNotValid("US45920010147"); // Fails on length verification (too long)
    }

    @Test
    public void testInvalidISINCrtlKey() throws Exception {
        isNotValid("FR0000133303"); // Fails on control key verification
    }

    private void isValid(String isin) {
        assertTrue("Expected a valid ISIN.", validator.isValid(isin, null));
    }

    private void isNotValid(String isin) {
        assertFalse("Expected an invalid ISIN.", validator.isValid(isin, null));
    }
}
