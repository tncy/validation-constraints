package net.tncy.validator.util;

import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author x-roy
 */
public class LuhnFormulaTest {
    
    public LuhnFormulaTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    @Test
    public void testValidChecksum() {
        isValidChecksum(972487086);
        isValidChecksum(8763);
        isValidChecksum(87630000);
        isValidChecksum(876300);
        isValidChecksum(543215);
        isValidChecksum(new int[]{7,9,9,2,7,3,9,8,7,1,3});
        
    }
    
    @Test
    public void testInvalidChecksum() {
        isNotValidChecksum(972487087);
        isNotValidChecksum(1111);
    }

    private void isValidChecksum(int number) {
        assertTrue("Expected a checksum.", LuhnFormula.isValid(number));
    }
    
    private void isValidChecksum(int[] number) {
        assertTrue("Expected a checksum.", LuhnFormula.isValid(number));
    }
    
    private void isNotValidChecksum(int number) {
        assertFalse("Expected an invalid checksum.", LuhnFormula.isValid(number));
    }
    
    private void isNotValidChecksum(int[] number) {
        assertFalse("Expected an invalid checksum.", LuhnFormula.isValid(number));
    }
   
    @Test
    public void testComputeChecksumDigit() {
        checkChecksum(972487086, 1);
        checkChecksum(97248708, 6);
        checkChecksum(876, 3);
        checkChecksum(54321, 5);
        checkChecksum(1111, 4);
        checkChecksum(new int[]{7,9,9,2,7,3,9,8,7,1}, 3);
    }
    
    private void checkChecksum(int number, int expected) {
        assertEquals("Not expected checksum value.", LuhnFormula.computeChecksumDigit(number), expected);
    }
    
    private void checkChecksum(int[] number, int expected) {
        assertEquals("Not expected checksum value.", LuhnFormula.computeChecksumDigit(number), expected);
    }
        
}